import React, { Component } from 'react';
import { BrowserRouter as Router, Route, NavLink, Switch, Redirect } from 'react-router-dom';
import HomeContainer from './components/home.component';
import ChatContainer from './components/chat.component';
import Auth from './components/auth/auth.component';
import './styles/top-navigation.css'

class App extends Component {

  // componentDidMount() {
  //   let onlineStatus = document.querySelector('.online-status');
  //   if(window.navigator.onLine) {
  //     onlineStatus.classList.toggle('offline' , false)
  //   } else {
  //     onlineStatus.classList.toggle('offline' , true)
  //   }
  //   window.addEventListener('offline', function(e) { 
  //     console.log('offline');
  //     onlineStatus.classList.toggle('offline' , true)
  //   });
  //   window.addEventListener('online', function(e) { 
  //     console.log('online');
  //     onlineStatus.classList.toggle('offline' , false)
  //   });
  // }
  //  className={`online-status ${window.navigator.onLine ? "" : "offline"}`}

  render() {
    return (
      <Router basename="/react-chat">
        <div className="content-wrapper">
          <nav className="top-navigation">
            {!this.props.user.token ? 
                <ul>
                  <li><NavLink exact activeClassName="active" to="/">Home</NavLink></li>
                  <li><NavLink activeClassName="active" to="/chat">Chat</NavLink></li>
                  <li><NavLink activeClassName="active" to="/auth/login">Log in</NavLink></li>
                  <li><NavLink activeClassName="active" to="/auth/signup">Sign up</NavLink></li>
                </ul>
                :
                <ul>
                  <li><NavLink exact activeClassName="active" to="/">Home</NavLink></li>
                  <li><NavLink exact activeClassName="active" to="/chat">Chat</NavLink></li>
                  <li><NavLink activeClassName="active" to="/chat/friends">Users</NavLink></li>
                </ul>
            }
          </nav>

          <div className="app-container">
            <Switch>
              <Route exact path="/" component={HomeContainer}/>
              <Route path="/chat" render={({ ...rest }) =>
                this.props.user.token ? 
                <ChatContainer {...rest}/> :  
                <Redirect to={{pathname: '/auth/login'}}/>
              }/>
              <Route path="/auth" component={Auth}/>
              <Route render={() => <h2>Page not found</h2>}/>
            </Switch> 
          </div>
        </div>
      </Router>
    );
  }
}

import { connect } from 'react-redux';

function mapStateToProps(state) {
  return {
    user: state.auth.user
  };
}

const AppContainer = connect(mapStateToProps)(App);

export default AppContainer;