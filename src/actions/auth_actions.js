import ActionTypes from '../constants/action_types';
import {reset} from 'redux-form';

export {
  loginUser,
  logoutUser,
  signupUser
};

function loginUser(loginData) {
  return dispatch => {
    dispatch(loginUserSuccess(loginData))
    dispatch(reset('login'))
  }
}


function loginUserSuccess(user) {
    return {
        type: 'LOGIN_USER_SUCCESS',
        user
    };
}

function logoutUser() {
  return {
    type: ActionTypes.LOGOUT_USER
  };
}

function signupUser(signupData) {
    return dispatch => {
    dispatch(signupUserSuccess(signupData))
    dispatch(reset('signup'))
  }
}

function signupUserSuccess(user) {
  return {
    type: ActionTypes.SIGNUP_USER_SUCCESS,
    user
  };
}