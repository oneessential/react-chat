import ActionTypes from '../constants/action_types';
import CONFIG from '../../config.json';

export {
  fetchMessages,
  onMessage,
  joinChat,
  leaveChat,
};

function fetchMessages(daysBefore) {
  var now = new Date();
  var startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate());
  var oneDayTimeStamp = 1000 * 60 * 60 * 24;
  var timestamp = daysBefore ? 
                  +startOfDay - oneDayTimeStamp * daysBefore 
                  : +startOfDay;
    
  return (dispatch, getState) => {
    dispatch(fetchMessagesLoading(true));
    // let userId = JSON.parse(localStorage.getItem('userData')).user._id;
    let state = getState();
    let userId = state.auth.user.user._id;
    fetch(CONFIG.API_GET_ROOMS_MESSAGES + userId + '?from=' + timestamp)
      .then((response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        dispatch(fetchMessagesLoading(false));
        return response;
      })
      .then(response => response.json())
      .then(messages => dispatch(fetchMessagesSuccess(messages)))
      // .catch(() => dispatch(fetchMessagesErrored(true)));
  };
}

function fetchMessagesSuccess(messages) {
  return {
    type: ActionTypes.FETCH_MESSAGES_SUCCESS,
    messages
  };
}

function fetchMessagesLoading(isLoading) {
  return {
    type: ActionTypes.FETCH_MESSAGES_LOADING,
    isLoading
  };
}

function onMessage(message) {
  return {
    type: ActionTypes.ON_MESSAGE,
    message
  };
}

function joinChat(username) {
  return {
    type: ActionTypes.JOIN_CHAT,
    username
  };
}

function leaveChat(username) {
  return {
    type: ActionTypes.LEAVE_CHAT,
    username
  };
}
