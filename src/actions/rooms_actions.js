import ActionTypes from '../constants/action_types';
import CONFIG from '../../config.json';

export {
  fetchRooms,
  createRoom,
  deleteRoom,
  deleteRoomSuccess,
  unreadMessages,
  readMessages,
  fetchOnline
};

function fetchOnline() {
  return (dispatch, getState) => {
    // dispatch(fetchRoomsLoading(true));
    let state = getState();
    let userId = state.auth.user.user._id;
    // let userId = JSON.parse(localStorage.getItem('userData')).user._id;
    fetch(CONFIG.API_GET_ONLINE + userId)
      .then((response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        //dispatch(fetchRoomsLoading(false));
        return response;
      })
      .then(response => response.json())
      .then(response => dispatch(fetchOnlineSuccess(response)))
  };
}

function fetchOnlineSuccess(online) {
  return {
    type: ActionTypes.FETCH_ONLINE_SUCCESS,
    online
  };
}

function fetchRooms() {
  return (dispatch, getState) => {
    dispatch(fetchRoomsLoading(true));
    let state = getState();
    let userId = state.auth.user.user._id;
    // let userId = JSON.parse(localStorage.getItem('userData')).user._id;
    fetch(CONFIG.API_GET_ROOMS + userId)
      .then((response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        dispatch(fetchRoomsLoading(false));
        return response;
      })
      .then(response => response.json())
      .then(response => dispatch(fetchRoomsSuccess(response)))
  };
}

function fetchRoomsSuccess(rooms) {
  return {
    type: ActionTypes.FETCH_ROOMS_SUCCESS,
    rooms
  };
}

function fetchRoomsLoading(isLoading) {
  return {
    type: ActionTypes.FETCH_ROOMS_LOADING,
    isLoading
  };
}

function createRoom(roomData) {
  function delay(t) {
   return new Promise(function(resolve) { 
       setTimeout(resolve, t)
   });
  }
  return (dispatch) => {
    // dispatch(createRoomLoading(true));
    let myHeaders = new Headers();
    myHeaders.set("Content-Type", "application/json");
    let myInit = { method: 'POST',
                  headers: myHeaders,
                  mode: 'cors',
                  body: JSON.stringify(roomData) 
                  };
    let createRoomRequest = new Request(CONFIG.API_ROOMS, myInit);
    fetch(createRoomRequest)
      .then((response) => {
          if (!response.ok) {
              throw Error(response.statusText);
          }
          // dispatch(createRoomLoading(false));
      })
      .then(() => {dispatch(createRoomSuccess(true))
              window.socket.emit('create-room', roomData)
              dispatch(fetchOnline())
              return delay(1000).then(dispatch(fetchRooms()))
            }
      )
      // .catch(() => dispatch(loginUserErrored(true)));
  };
}

function createRoomSuccess() {
  return {
    type: ActionTypes.CREATE_ROOM_SUCCESS
  };
}

// function createRoomLoading(isLoading) {
//   return {
//     type: ActionTypes.CREATE_ROOM_LOADING,
//     isLoading
//   };
// }

function deleteRoom(roomId) {
  function delay(t) {
   return new Promise(function(resolve) { 
       setTimeout(resolve, t)
   });
  }
  return (dispatch) => {
    // dispatch(deleteRoomLoading(true));
    let myHeaders = new Headers();
    myHeaders.set("Content-Type", "application/json");
    let myInit = { method: 'DELETE',
                  headers: myHeaders,
                  mode: 'cors'
                  };
    let deleteRoomRequest = new Request(CONFIG.API_ROOMS + roomId, myInit);
    fetch(deleteRoomRequest)
      .then((response) => {
          if (!response.ok) {
              throw Error(response.statusText);
          }
          // dispatch(deleteRoomLoading(false));
      })
      .then(() => {dispatch(deleteRoomSuccess(true))
              window.socket.emit('delete-room', roomId)
              return delay(1000).then(dispatch(fetchRooms()))
            }
      )
  };
}

function deleteRoomSuccess(status) {
  return {
    type: ActionTypes.DELETE_ROOM_SUCCESS,
    status
  };
}

function unreadMessages(roomId) {
  return {
    type: ActionTypes.UNREAD_MESSAGES,
    roomId
  };
}

function readMessages(roomId) {
  return {
    type: ActionTypes.READ_MESSAGES,
    roomId
  };
}