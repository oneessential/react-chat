import ActionTypes from '../constants/action_types';
import CONFIG from '../../config.json';

export {
  fetchUsers
};

function fetchUsers() {
  return (dispatch) => {
    dispatch(fetchUsersLoading(true));
    fetch(CONFIG.API_USERS)
      .then((response) => {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        dispatch(fetchUsersLoading(false));
        return response;
      })
      .then(response => response.json())
      .then(response => dispatch(fetchUsersSuccess(response)))
      // .catch(() => dispatch(fetchUsersErrored(true)));
  };
}

function fetchUsersSuccess(users) {
  return {
    type: ActionTypes.FETCH_USERS_SUCCESS,
    users
  };
}

function fetchUsersLoading(isLoading) {
  return {
    type: ActionTypes.FETCH_USERS_LOADING,
    isLoading
  };
}
