import { connect } from 'react-redux';
// import { getInvite } from '../actions/get_invite';
import App from './App.js';

function mapStateToProps(state) {
  return {
    users: state.users,
    messages: state.messages
  };
}

function mapDispatchToProps(dispatch) {
  return {
    // onGetInvite: () => dispatch(getInvite()),
    // onAddToInvite: (name) => dispatch(addToInvite(name))
  };
}

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;