import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import LoginContainer from './login.container';
import SignupContainer from './signup.container';
import '../../styles/auth.css';

const Auth = ({ match }) => (
  <div className="auth-wrapper">
    <div className="tabs-content">
      <Switch>
        <Route path={`${match.url}/login`}  component={LoginContainer}/>
        <Route path={`${match.url}/signup`}  component={SignupContainer}/>
        <Redirect from="/auth" to="/auth/login"/>
      </Switch>
    </div>
  </div>
);

export default Auth