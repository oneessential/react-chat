import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoginForm from './login-form.component';
import { loginUser } from '../../actions/auth_actions';
import { SubmissionError } from 'redux-form';
import CONFIG from '../../../config.json';
import { withRouter } from 'react-router'

class Login extends Component {
	constructor(props) {
		super(props)

		this.loginFormSubmit = this.loginFormSubmit.bind(this)
	}

handleResponse(response) {
  if (!response.ok) {
		return response.json()
			.then((response) => {throw new SubmissionError({ responseErrors: response.message, _error: 'Signup failed!' })})
    
  } else {
    return response.json();
  }
}

loginFormSubmit(loginData) {
	let myHeaders = new Headers();
    myHeaders.set("Content-Type", "application/json");
    let myConfig = { 
					method: 'POST',
					headers: myHeaders,
					mode: 'cors',
					body: JSON.stringify(loginData)
                   };
    let loginRequest = new Request(CONFIG.API_LOGIN, myConfig);
    return fetch(loginRequest)
		.then(this.handleResponse)
		.then(data => {
			localStorage.setItem('userData', JSON.stringify(data))
			this.props.onLoginUser(data)
			this.props.history.push('/chat')
		})
  }


	render() {
		return (
			<div className="tab-login">
				<LoginForm onSubmit={this.loginFormSubmit} />
			</div>
		)
	}
}

function mapDispatchToProps(dispatch) {
	return {
		onLoginUser: (loginData) => dispatch(loginUser(loginData))
	};
}

export default withRouter(connect(null, mapDispatchToProps)(Login));