import React from 'react'
import { Field, reduxForm } from 'redux-form'
import validate from './validate-helper'


const renderField = ({ classnames, input, label, type, meta: { touched, error } }) => (
  <div className="input-group">
    <label>{label}
    <div>
      <input {...input} placeholder={label} type={type}/>
      {touched && ((error && <span className={`error ${classnames}`}>{error}</span>))}
    </div>
    </label>
  </div>
)

const SignupForm = (props) => {
  const { handleSubmit, pristine, reset, submitting } = props
  return (
    <form onSubmit={handleSubmit} autoComplete="off">
      <Field name="responseErrors" type="hidden" component={renderField} classnames="response-errors"/>
      <Field name="firstName" type="text" component={renderField} label="First name"/>
      <Field name="lastName" type="text" component={renderField} label="Last name"/>
      <Field name="email" type="email" component={renderField} label="Email"/>
      <Field name="password" type="password" component={renderField} label="Password"/>
      <Field name="confirmPassword" type="password" component={renderField} label="Confirm password" />
      <div className="input-group">
        <input type="submit" disabled={submitting} value="Submit"/>
        <input type="button" disabled={pristine || submitting} onClick={reset} value="Clear Values" />
      </div>
    </form>
  )
}

export default reduxForm({
  form: 'signup',
  validate: validate.signup
})(SignupForm)