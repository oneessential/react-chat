import React, { Component } from 'react';
import { connect } from 'react-redux';
import SignupForm from './signup-form.component';
import { signupUser } from '../../actions/auth_actions';
import { SubmissionError } from 'redux-form';
import CONFIG from '../../../config.json';
import { withRouter } from 'react-router'

class Signup extends Component {
	constructor(props) {
		super(props)

		this.signupFormSubmit = this.signupFormSubmit.bind(this)
	}

handleResponse(response) {
  if (!response.ok) {
	return response.json()
		.then((response) => {throw new SubmissionError({ responseErrors: response.message, _error: 'Signup failed!' })})
    
  } else {
    return response.json();
  }
}

signupFormSubmit(signupData) {
	let myHeaders = new Headers();
    myHeaders.set("Content-Type", "application/json");
    let myConfig = { 
					method: 'POST',
					headers: myHeaders,
					mode: 'cors',
					body: JSON.stringify(signupData)
                   };
    let signupRequest = new Request(CONFIG.API_SIGNUP, myConfig);
    return fetch(signupRequest)
		.then(this.handleResponse)
		.then(data => {
			localStorage.setItem('userData', JSON.stringify(data))
			this.props.onSignupUser(data)
			this.props.history.push('/chat')
		})
  }

	render() {
		return <SignupForm onSubmit={this.signupFormSubmit} />;
	}
}

function mapDispatchToProps(dispatch) {
	return {
		onSignupUser: (signupData) => dispatch(signupUser(signupData))
	};
}

export default withRouter(connect(null, mapDispatchToProps)(Signup));