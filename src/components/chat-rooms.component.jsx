import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Loader from './loader.component'
import '../styles/chat-rooms.css'

class ChatRooms extends Component {
  constructor(props) {
    super(props);

    this.hostUser = this.props.user.user
    this.onReadMessages = this.onReadMessages.bind(this)
  }

  capitalize(text) {
    if (typeof text === "object") {
      text = text[0]
    }
    return text
      .split(' ')
      .map(n => n.charAt(0).toUpperCase() + n.slice(1))
      .join(' ')
  }

  lastMesage(roomId) {
    let messages = this.props.messages || [];
    let data;
    //filter messages by roomId
    let messagesForRoom = messages.filter(message =>
      message.roomId === roomId && message.user._id !== this.hostUser._id)
    if (!messages || !messagesForRoom.length) {
      return data = {
        msg: "",
        time: "time"
      }
    }
    // return last message text
    let lastMessage = messagesForRoom.filter(message =>
      message.created_at === (
        Math.max.apply(null, messagesForRoom.map(message => message.created_at)))
    )[0]
    data = {
      msg: lastMessage.msg,
      time: new Date(lastMessage.created_at).toLocaleTimeString('uk-UA', { hour: '2-digit', minute: '2-digit' })
    }
    return data
  }

  onReadMessages(e) {
    this.props.onReadMessages(e.currentTarget.dataset.roomid)
  }

  render() {
    let rooms = this.props.rooms || [];
    let unread = this.props.unread || [];
    let online = this.props.online || [];

    return (
      <Loader loading={this.props.isLoading}>
        <div className="chat-list">
          <ul className="contacts">
            {rooms.length ? rooms.map(room => {
              let chatName = room.names.length ?
                this.hostUser.username === room.names[0] ?
                  this.capitalize(room.names[1]) :
                  this.capitalize(room.names[0])
                : room._id;
              let friendId = room.members.filter(member => member !== this.hostUser._id)[0];
              return <li key={'room_id:' + room._id}
                className={`contacts-item ${unread.includes(room._id)} 
                        ${online.includes(friendId) ? "online" : ""}`}>
                <NavLink activeClassName="active"
                  to={`${this.props.match.url}/${room._id}`}
                  data-roomid={room._id}
                  onClick={this.onReadMessages}>
                  <div className="contacts-item__image">
                    <img src={"https://api.adorable.io/avatars/52/" + chatName + ".png"}
                      alt={room._id + " avatar"} />
                  </div>
                  <div className="contacts-item__content">
                    <h4>{chatName}</h4>
                    <p>{this.lastMesage(room._id).msg}</p>
                  </div>
                  <div className="contacts-item__status">
                    {this.lastMesage(room._id).time}
                  </div>
                </NavLink>
              </li>
            })
              : (
                <div>
                  <li className="empty">Friend <br />list <br />is <br />empty</li>
                  <NavLink className="empty"
                    to={`${this.props.match.url}/friends`}>Find friends
                  </NavLink>
                </div>
              )}
          </ul>
        </div>
      </Loader>
    )
  }
}

import { connect } from 'react-redux';
import { readMessages } from '../actions/rooms_actions';

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    unread: state.rooms.unread
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onReadMessages: (roomId) => dispatch(readMessages(roomId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatRooms);
