import React, { PureComponent } from 'react';
import Search from './search.component';
import Loader from './loader.component';

class ChatUsers extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
			search: ''
		}

    this.hostUser = this.props.user.user;
    this.createRoomHandler = this.createRoomHandler.bind(this);
  }

  searchHandler(e) {
    this.setState({
      search: e.target.value
    })
  }

	createRoomHandler(e) {
    let roomData = {
      firstId: this.hostUser._id,
      secondId: e.target.dataset.userid
    }
    this.props.onCreateRoom(roomData)
	}

  render() {
    //array of friends id's for current user
	const rooms = this.props.rooms || [];

    let friends = [this.hostUser._id];
    rooms.map(room =>
                    room.members.map(member =>
                        friends.push(member)
                      )
                  );

		let users = this.props.users || [];

    //array of all users, not friends
    users = users.filter(user => !friends.includes(user._id))

    if (this.state.search) {
      users = users.filter(user =>
        user.username.toLowerCase()
        .includes(this.state.search.toLowerCase())
      )
    }

    users = users
      .map(user =>
        <li key={user._id} className="search-result__item">
          <p>{user.username}
            <br/>
            <span>{user.email}</span>
          </p>
					<a href="#" data-userid={user._id} onClick={this.createRoomHandler} title="Create conversation"></a>
				</li>
    )

    return (
			  <Loader loading={this.props.isLoading}>
            <Search onSearch={this.searchHandler.bind(this)} class="friends__search"/>
            <ul className="friends__search-result">
                {users.length ? users : <li>No new users. Try to load</li>}
            </ul>
        </Loader>
    )
  }
}


import { connect } from 'react-redux';
import { createRoom } from '../actions/rooms_actions';

function mapStateToProps(state) {
  return {
    user: state.auth.user,
    users: state.users.users,
    rooms: state.rooms.rooms
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onCreateRoom: (roomData) => dispatch(createRoom(roomData))
  };
}

const ChatUsersContainer = connect(mapStateToProps, mapDispatchToProps)(ChatUsers);

export default ChatUsersContainer;
