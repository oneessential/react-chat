import React, {PureComponent} from 'react';
import { Route, Switch } from 'react-router-dom';
import Search from './search.component';
import ChatRooms from './chat-rooms.component';
import MessageHistoryContainer from './message-history.component';
import MessageInput from './message-input.component';
import FriendsContainer from './friends.component';
import CONFIG from '../../config.json';
import * as io from 'socket.io-client';
import { withRouter } from 'react-router';
import { toast } from 'react-toastify';
import incomingMessage from './sound.wav';
import online from './online.mp3';
import offline from './offline.mp3';

class Chat extends PureComponent {
  constructor(props){
    super(props);
    this.state = {}

    this.match = this.props.match;
    this.sendHendler = this.sendHendler.bind(this);
  }

  componentDidMount() {
    if (this.props.user.token) {
      this.props.onFetchMessages();
      this.props.onFetchRooms();
      this.props.onFetchOnline();
    }
    
    if(!window.socket && this.props.user.token) {
      this.createSocketConnection()
    }

    //aside toggle logic
    const aside = document.querySelector('.chat-wrapper aside');
    const arrow = document.querySelector('.aside-navigation__arrow');

    arrow.addEventListener('click', () => 
      aside.classList.toggle('is-open')
    )

    let onload = function(){
      if(window.innerWidth > 780){
        aside.classList.toggle('is-open', true);
      } else {
        aside.classList.toggle('is-open', false);
      }
    };

    onload();

    let id;
    let doneResizing = () => {
      if(window.innerWidth < 780) {
        if(aside.classList.contains('is-open')) {
          aside.classList.remove('is-open');
        }
      } 
      else if (window.innerWidth > 780) {
        if (!aside.classList.contains('is-open')) {
          aside.classList.add('is-open');
        }
      }
    }
   let onResize = () => {
      clearTimeout(id);
      id = setTimeout(doneResizing, 500);
    }
    window.addEventListener('resize', onResize);
  }

  PlaySound(type) {
    let snd = new Audio(type);
    snd.play();
  }

  createSocketConnection() {
    window.socket = io.connect(CONFIG.API_URL)
    window.socket.on('connect', () => {
          console.log('connected');
          window.socket.emit('authenticate', { token: this.props.user.token })
    })
    
    window.socket.on('message', msg => {
      this.props.onMessage(msg)
      let friendsRooms = [];
      this.props.rooms.map(room => friendsRooms.push(room._id));
      if(friendsRooms.includes(msg.roomId)) {
        if( msg.user._id !== this.props.user.user._id 
            && window.location.pathname.split('/').pop() !== msg.roomId) {
          this.props.onUnreadMessages(msg.roomId)
          this.PlaySound(incomingMessage)
          friendsRooms = []
        }
      }
    })

    window.socket.on('join', msg => {
      this.props.onJoinChat(msg.user.username)      
      if(this.props.rooms.filter(room => room.members.includes(msg.user._id)).length 
        && this.props.user.user._id !== msg.user._id) {
        this.props.onFetchOnline();
        this.PlaySound(online)
        toast.success(this.capitalize(msg.user.username) + " : online")
      }
    })

    window.socket.on('leave', msg => {
      this.props.onLeaveChat(msg.user.username)      
      if(this.props.rooms.filter(room => room.members.includes(msg.user._id)).length 
        && this.props.user.user._id !== msg.user._id) {
        this.props.onFetchOnline();
        this.PlaySound(offline)
        toast.error(this.capitalize(msg.user.username) + " : offline")
      }
    })

    window.socket.on('create-room', secondUserId => {
      if(secondUserId === this.props.user.user._id) {
        this.props.onFetchRooms()
        this.props.onFetchOnline();
        toast.success("Room created")
        console.log('refresh room list => create-room')
      }
    })

    window.socket.on('delete-room', roomId => {
      if(this.props.rooms.filter(room => room._id === roomId).length
      && this.props.rooms.filter(room => room._id === roomId)[0].members.includes(this.props.user.user._id)) {
        if(window.location.pathname.split('/').pop() === roomId) {
          this.props.history.push('/chat')
          this.props.onFetchRooms()
          toast.error("Room deleted")
          console.log('refresh room list => delete-room')
        } else {
          this.props.onFetchRooms()
          toast.error("Room deleted")
          console.log('refresh room list => delete-room')
        }
      }
    })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize);
  }

  componentWillMount() {
    if (!this.props.user.token) {
      this.props.history.push('/auth/login')
    }
  }
  
  searchHandler(e) {
    this.setState({
      search: e.target.value
    })
  }

  capitalize(text) {
    if(typeof text === "object") {
      text = text[0]
    }
    return text
            .split(' ')
            .map(n => n.charAt(0).toUpperCase() + n.slice(1))
            .join(' ')
  }

  sendHendler(message, roomId) {
    window.socket.emit('message', message, roomId);
  }

  render() {
    let rooms = this.props.rooms;
    if (this.state.search) {
      rooms = rooms.filter(room => 
        room.names
          .filter(name => name !== this.props.user.user.username)
          .join()
          .toLowerCase()
          .includes(this.state.search.toLowerCase())  
      )
    }
    return (
      <div className="chat-wrapper">
        <aside className="aside-wrapper">
          <nav className="aside-navigation">
            <div className="aside-navigation__arrow"></div>

            <Search onSearch={this.searchHandler.bind(this)} class="search-chat"/>

          </nav>

          <ChatRooms 
            rooms={rooms}
            online={this.props.online}
            match={this.props.match} 
            messages={this.props.messages} 
            isLoading={this.props.isLoading} />

        </aside>
        <main>
          <Switch>
            <Route path={`${this.match.url}/friends`}  render={({ match }) => (
              <div className="chat-history">
                <FriendsContainer />
              </div>
            )}/>
            <Route path={`${this.match.url}/:id`}  render={({ match }) => (
              <div className="chat-history">
                <MessageHistoryContainer 
                  messages={this.props.messages} 
                  roomId={ match.params.id }/>
                <MessageInput 
                  onSendMessage={this.sendHendler} 
                  roomId={ match.params.id }/>
                
              </div>
            )}/>
            <Route exact path={this.match.url} render={() => (
              <h3>Select chat room</h3>
            )}/>
            <Route render={() => <h1>No such contact history</h1>}/>
          </Switch>
        </main>
      </div>
    );
  }
}

import { connect } from 'react-redux';
import { 
        fetchMessages,
        onMessage,
        joinChat,
        leaveChat
      } from '../actions/messages_actions';
import { fetchRooms, unreadMessages, fetchOnline } from '../actions/rooms_actions';

function mapStateToProps(state) {
  return {
    rooms: state.rooms.rooms,
    messages: state.messages.messages,
    user: state.auth.user,
    isLoading: state.rooms.isLoading,
    online: state.rooms.online
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchMessages: (daysBefore) => dispatch(fetchMessages(daysBefore)),
    onMessage: (message) => dispatch(onMessage(message)),
    onJoinChat: (username) => dispatch(joinChat(username)),
    onLeaveChat: (username) => dispatch(leaveChat(username)),
    onFetchRooms: () => dispatch(fetchRooms()),
    onUnreadMessages: (roomId) => dispatch(unreadMessages(roomId)),
    onFetchOnline: () => dispatch(fetchOnline())
  };
}

const ChatContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(Chat));

export default ChatContainer;