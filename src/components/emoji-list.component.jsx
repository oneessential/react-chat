import React, { Component } from 'react';
import Emojify from 'react-emojione';

export default class EmojiList extends Component {
    constructor(props) {
        super(props)

        this.emojiClickHandler = this.emojiClickHandler.bind(this)
    }

    emojiClickHandler(e) {
        this.props.onEmojiClick(e.target.title)
    }

    render() {
        return (
            <Emojify style={{ height: 32, width: 32 }} onClick={this.emojiClickHandler}>
                :grinning::smiley::smile::grin::laughing::sweat_smile::joy::rofl::relaxed:
                :blush::innocent::slight_smile::upside_down::wink::relieved::heart_eyes:
                :kissing_heart::kissing::kissing_smiling_eyes::kissing_closed_eyes::yum:
                :stuck_out_tongue_winking_eye::stuck_out_tongue_closed_eyes::stuck_out_tongue:
                :money_mouth::hugging::nerd::sunglasses::clown::cowboy::smirk::unamused:
                :disappointed::pensive::worried::confused::slight_frown::frowning2::persevere:
                :confounded::tired_face::weary::triumph::angry::rage::no_mouth::neutral_face:
                :expressionless::hushed::frowning::anguished::open_mouth::astonished::dizzy_face:
                :flushed::scream::fearful::cold_sweat::cry::disappointed_relieved::drooling_face:
                :sob::sweat::sleepy::sleeping::rolling_eyes::thinking::lying_face::grimacing:
                :zipper_mouth::nauseated_face::sneezing_face::mask::thermometer_face::head_bandage:
                :dog::cat::mouse::hamster::rabbit::fox::bear::panda_face::koala::tiger::lion_face:
                :cow::pig::pig_nose::frog::monkey_face::see_no_evil::hear_no_evil::speak_no_evil:
                :monkey:
            </Emojify>
        )
    }
};
