import React, { Component } from 'react';
import ChatUsersContainer from './chat-users.component';
import '../styles/chat-users.css'

class Friends extends Component {
  constructor(props){
    super(props);

    this.fetchUsers = this.fetchUsers.bind(this)
  }

  fetchUsers() {
    this.props.onFetchUsers()
  }

   capitalize(text) {
    if(typeof text === "object") {
      text = text[0]
    }
    return text
            .split(' ')
            .map(n => n.charAt(0).toUpperCase() + n.slice(1))
            .join(' ')
  }

  render() {
    return (     
        <div className="friends">
          <p className="friends__greeting">Hello {this.capitalize(this.props.user.user.username)}, here you can find friends and create chat rooms with them</p>
          <button className="friends__load-users" onClick={this.fetchUsers}>Load users</button>
          <ChatUsersContainer isLoading={this.props.isLoading}/>
        </div>
    )
  }
}

import { connect } from 'react-redux';
import { fetchUsers } from '../actions/users_actions';

function mapStateToProps(state) {
  return {
    isLoading: state.users.isLoading,
    user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchUsers: () => dispatch(fetchUsers())
  };
}

const FriendsContainer = connect(mapStateToProps, mapDispatchToProps)(Friends);

export default FriendsContainer;