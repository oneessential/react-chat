import React, { Component } from 'react';

class Home extends Component {

  render() {
    let greeting = "Wellcome to Chat app.";

    if(this.props.user.user) {
      // let username = JSON.parse(localStorage.getItem('userData')).user.username;
      // greeting = username.charAt(0).toUpperCase() + username.slice(1) + " wellcome to Chat app";
      let userNamesToUppercase = this.props.user.user.username
                      .split(' ')
                      .map(n => n.charAt(0).toUpperCase() + n.slice(1))
                      .join(' ')

      greeting = userNamesToUppercase + " wellcome to Chat app";
    }

    let logoutButton;
    if(this.props.user.token) {
      logoutButton = <button onClick={this.props.onLogoutUser}>Logout</button>
    }
    return (
      <div className="home-wrapper">
        <h2>{ greeting }</h2>
        { logoutButton }
      </div>
    );
  }
}

import { connect } from 'react-redux';
import { logoutUser } from '../actions/auth_actions';

function mapStateToProps(state) {
  return {
    user: state.auth.user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onLogoutUser: () => dispatch(logoutUser())
  };
}

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Home);

export default HomeContainer;
