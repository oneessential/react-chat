import React from 'react';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';

const Loader = ({ loading, children}) => {
    const loader = (
        <svg className="spinner" key="loader" viewBox="0 0 50 50">
            <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="2"></circle>
        </svg>
    )

    return (
        <CSSTransitionGroup 
            className="stage"
            transitionName="crossfade"
            transitionAppear={true} 
            transitionAppearTimeout={500}
            transitionEnterTimeout={500} 
            transitionLeaveTimeout={300}
            component="div"
            aria-busy={loading}
        >
            {loading ? loader : children}
        </CSSTransitionGroup>
    );
}

export default Loader;