import React, { Component } from 'react';
import '../styles/message-history.css';

class MessageHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moreMessages: false
    }

    this.moreMessagesToggle = this.moreMessagesToggle.bind(this)
  }

  capitalize(text) {
    if(typeof text === "object") {
      text = text[0]
    }
    return text
            .split(' ')
            .map(n => n.charAt(0).toUpperCase() + n.slice(1))
            .join(' ')
  }

  moreMessagesToggle() {
    let status = this.state.moreMessages;
    this.setState({
      moreMessages: !status
    })
  }

  render() {
    let rooms = this.props.rooms || []
    let roomName = rooms
                    .filter(room => room._id === this.props.roomId)[0].names
                    .filter(name => name !== this.props.user.user.username)
                    || this.props.roomId
         
    return (
          <div className="messages-header">
            <div className={`messages-header__more-messages ${this.state.moreMessages ? "active" : ""}`} 
                 title="Load messages for past days"
                 onClick={this.moreMessagesToggle}>
              <ul className="messages-header__more-messages-list">
                <li onClick={this.props.onDaysBefore} data-days="">today</li>
                <li onClick={this.props.onDaysBefore} data-days="1">tomorrow</li>
                <li onClick={this.props.onDaysBefore} data-days="3">3 days</li>
                <li onClick={this.props.onDaysBefore} data-days="5">5 days</li>
              </ul>
            </div>
            <div className="messages-header__name">{this.capitalize(roomName)}</div>
            <div className="messages-header__leave" 
                 title="Leave this conversation"
                 onClick={this.props.onLeaveRoom}
                 data-roomid={this.props.roomId}
            ></div>
          </div>
    )
  }
}

export default MessageHeader;