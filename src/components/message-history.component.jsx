import React, { Component } from 'react';
import MessageItem from './message-item.component';
import MessageHeader from './message-header.component';
import Loader from './loader.component'
import '../styles/message-history.css'
import { withRouter } from 'react-router'

class MessageHistory extends Component {
  constructor(props){
    super(props);

    this.daysBeforeFetchMessages = this.daysBeforeFetchMessages.bind(this);
    this.leaveRoomHandler = this.leaveRoomHandler.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.deleteRoom !== this.props.deleteRoom) {
      this.props.deleteRoomStatus(false)
      this.props.history.push("/chat")
    }
  }

  componentDidMount() {
    if(!this.props.isLoading) {
      const scrollArea = document.querySelector('.messages__list');
      scrollArea.scrollTop = scrollArea.scrollHeight;
    }
  }

  componentDidUpdate() {
    if(!this.props.isLoading) {
      const scrollArea = document.querySelector('.messages__list');
      scrollArea.scrollTop = scrollArea.scrollHeight;
    }
  }

  daysBeforeFetchMessages(event) {
    this.props.onFetchMessages(event.target.dataset.days);
  }

  leaveRoomHandler(event) {
    let confirmText = "Are you shure to leave this conversation? \nAll messages will be DELETED too"
    if(confirm(confirmText)) {
      this.props.onDeleteRoom(event.target.dataset.roomid);
    }
  }

  render() {
    let messages = this.props.messages || [];

    if(messages) {
      messages = this.props.messages
        .filter(message =>
          typeof message.msg !== 'object'
          && message.roomId === this.props.roomId)
        .map((message, index) =>
          <MessageItem message={ message } key={'msg_id:' + message._id} user={this.props.user}/>);
    }

    return (
        <div className="messages">
          <MessageHeader roomId={this.props.roomId} 
                         onDaysBefore={this.daysBeforeFetchMessages}
                         onLeaveRoom={this.leaveRoomHandler}
                         rooms={this.props.rooms}
                         user={this.props.user}
          />
          <Loader loading={this.props.isLoading}>
            <ul className="messages__list">
              { messages.length ? messages : <li className="no-messages">No messages for this period. Write some or load from history</li> }
            </ul>
          </Loader>
        </div>
    )
  }
}
MessageHistory.defaultProps = { deleteRoom: false };

import { connect } from 'react-redux';
import { fetchMessages } from '../actions/messages_actions';
import { deleteRoom, deleteRoomSuccess } from '../actions/rooms_actions';

function mapStateToProps(state) {
  return {
    isLoading: state.messages.isLoading,
    deleteRoom: state.rooms.deleted,
    user: state.auth.user,
    rooms: state.rooms.rooms
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onFetchMessages: (daysBefore) => dispatch(fetchMessages(daysBefore)),
    onDeleteRoom: (roomId) => dispatch(deleteRoom(roomId)),
    deleteRoomStatus: (status) => dispatch(deleteRoomSuccess(status))
  };
}

const MessageHistoryContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(MessageHistory));

export default MessageHistoryContainer;
