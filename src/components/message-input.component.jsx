import React, { Component } from 'react';
import EmojiList from './emoji-list.component';
import '../styles/message-input.css';

class MessageInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      textInput: '',
      emojiListOpen: false
    }

    this.submitHandler = this.submitHandler.bind(this);
    this.textChangeHendler = this.textChangeHendler.bind(this);
    this.checkEnterHandler = this.checkEnterHandler.bind(this);

    this.onEmojiClick = this.onEmojiClick.bind(this)
    this.emojiListToggle = this.emojiListToggle.bind(this)
  }

  textChangeHendler(event) {
    this.setState({
      textInput: event.target.value
    })
  }

  submitHandler(event) {
    event.preventDefault();
    if (this.state.textInput.replace(/\s/g, ''))
    this.props.onSendMessage(this.state.textInput, this.props.roomId);

    this.setState({
      textInput: ''
    })
  }

  checkEnterHandler(event) {
    if (event.target.value.replace(/\s/g, ''))
    if (event.keyCode === 13 && event.shiftKey === false) {
      this.submitHandler(event);
    }
  }

  onEmojiClick(title) {
    this.setState((prevState) => {
      return {
        textInput: prevState.textInput + title,
        emojiListOpen: false
      }
    })
    this.refs.newMsg.focus()
  }

  emojiListToggle() {
    this.setState((prevState) => {
      return {emojiListOpen: !prevState.emojiListOpen}
    })
  }

  componentDidMount() {
    this.refs.newMsg && this.refs.newMsg.focus()
  }

  render() {
    return (
        <div className="chat-history__new-message">
          <div className={`emoji-container ${this.state.emojiListOpen ? "open" : ""}`}>
            <EmojiList onEmojiClick={this.onEmojiClick} 
                       onEmojiListToggle={this.state.emojiListOpen}/>
          </div>
          <form onSubmit={this.submitHandler}>
            <textarea onChange={this.textChangeHendler}
                      onKeyDown={this.checkEnterHandler} 
                      value={this.state.textInput}
                      ref="newMsg"
                      placeholder="Type a message here"></textarea>
            <div onClick={this.emojiListToggle} className={`smile-button ${this.state.emojiListOpen ? "open" : ""}`}></div>
            <input className="new-message__submit" type="submit" value=""/>
          </form>
        </div>
    )
  }
}

export default MessageInput;