import React, { Component } from 'react';
import {emojify} from 'react-emojione';

class MessageItem extends Component {

  render() {

    let message = this.props.message;
    return (     
      <li data-id={message._id} 
        className={
          "messages-item " + 
            (this.props.user.user.username === message.user.username 
              ? "messages-item--outgoing" 
              : "messages-item--incoming")
        }>
        <div className="messages-item__image">
          <img src={"https://api.adorable.io/avatars/120/"+ message.user.username +".png"} 
               alt={ message.user.username + " avatar" }/>
        </div>
        <div className="messages-item__content">
          { emojify(message.msg) }
        </div>
        <div className="messages-item__timestamp">
          { message.user.username } { new Date(message.created_at).toLocaleTimeString('uk-UA') }
        </div>
      </li>
    )
  }
}

export default MessageItem;