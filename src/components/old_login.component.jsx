import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      touched: {
        username: false,
        password: false,
      }
    }
    
    this.usernameChangeHendler = this.usernameChangeHendler.bind(this);
    this.passwordChangeHendler = this.passwordChangeHendler.bind(this);
    this.submitHendler = this.submitHendler.bind(this);
  }

  validate(username, password) {
    // var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // re.test(email)
    return {
      username: username.length === 0,
      password: password.length === 0
    };
  }

  usernameChangeHendler(event) {
    this.setState({
      username: event.target.value
    })
  }

  passwordChangeHendler(event) {
    this.setState({
      password: event.target.value
    })
  }

  handleBlur = (field) => (event) => {
    this.setState({
      touched: { ...this.state.touched, [field]: true },
    });
  }

  submitHendler(event) {
    event.preventDefault();

    if (!this.canBeSubmitted()) {
      event.preventDefault();
      return;
    }

    const loginData = {
      username: this.state.username,
      password: this.state.password
    }

    setTimeout(() => this.setState({
      username: '',
      password: ''
    }), 500)
    

    this.props.onLoginUser(loginData)
    setTimeout(() => this.props.history.push('/chat'), 600)
    
  }

  canBeSubmitted() {
    const errors = this.validate(this.state.username, this.state.password);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    return !isDisabled;
  }

  render() {
    const errors = this.validate(this.state.username, this.state.password);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    
    const shouldMarkError = (field) => {
      const hasError = errors[field];
      const shouldShow = this.state.touched[field];
      
      return hasError ? shouldShow : false;
    };

    return (
      <div className="tab-login">
        <form onSubmit={this.submitHendler}>
          <div className="input-group">
            <label>Username
              <input
                className={shouldMarkError('username') ? "error" : ""}
                onChange={this.usernameChangeHendler}
                onBlur={this.handleBlur('username')}
                value={this.state.username}
                type="text"
                placeholder="Username"
                required
            />
            </label>
          </div>

          <div className="input-group">
            <label>Password
              <input
                className={shouldMarkError('password') ? "error" : ""}
                onChange={this.passwordChangeHendler}
                onBlur={this.handleBlur('password')}
                value={this.state.password}
                type="password"
                placeholder="Password"
                required
                name="password"/>
            </label>
          </div>
          <div className="input-group">
            <input type="submit" className="submit-button" disabled={isDisabled} value="Log In"/>
          </div>
          <Link to="/auth/signup" className="create-acc">Don't have an account?</Link>
        </form>
      </div>
    );
  }
}

import { connect } from 'react-redux';
import { loginUser } from '../actions/auth_actions';

function mapDispatchToProps(dispatch) {
  return {
    onLoginUser: (loginData) => dispatch(loginUser(loginData))
  };
}

const LoginContainer = connect(null, mapDispatchToProps)(Login);

export default LoginContainer;