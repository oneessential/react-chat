import React, { Component } from 'react';

class Signup extends Component {
  constructor() {
  super();
  this.state = {
    username: '',
    password: '',
    touched: {
      username: false,
      password: false,
    }
  }
  
  this.usernameChangeHendler = this.usernameChangeHendler.bind(this);
  this.passwordChangeHendler = this.passwordChangeHendler.bind(this);
  this.submitHendler = this.submitHendler.bind(this);
}

validate(username, password) {
  return {
    username: username.length < 2,
    password: password.length < 6
  };
}

usernameChangeHendler(event) {
  this.setState({
    username: event.target.value
  })
}

passwordChangeHendler(event) {
  this.setState({
    password: event.target.value
  })
}

handleBlur = (field) => (event) => {
  this.setState({
    touched: { ...this.state.touched, [field]: true },
  });
}

submitHendler(event) {
    event.preventDefault();

    if (!this.canBeSubmitted()) {
      event.preventDefault();
      return;
    }

    const signupData = {
      username: this.state.username,
      password: this.state.password
    }

    this.setState({
      username: '',
      password: ''
    })

    this.props.onSignupUser(signupData)
    this.props.history.push('/auth/login')
  }

  canBeSubmitted() {
    const errors = this.validate(this.state.username, this.state.password);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    return !isDisabled;
  }

  render() {
    const errors = this.validate(this.state.username, this.state.password);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    
    const shouldMarkError = (field) => {
      const hasError = errors[field];
      const shouldShow = this.state.touched[field];
      
      return hasError ? shouldShow : false;
    };

    return (
      <div className="tab-signup">
        <form onSubmit={this.submitHendler}>
          <div className="input-group">
            <label>Username
              <input
                className={shouldMarkError('username') ? "error" : ""}
                onChange={this.usernameChangeHendler}
                onBlur={this.handleBlur('username')}
                value={this.state.username}
                type="text"
                placeholder="Username"
                required/>
            </label>
          </div>
          <div className="input-group">
            <label>Password
              <input
                className={shouldMarkError('password') ? "error" : ""}
                onChange={this.passwordChangeHendler}
                onBlur={this.handleBlur('password')}
                value={this.state.password}
                type="password"
                placeholder="Password"
                required/>
            </label>
          </div>
          <div className="input-group">
            <input type="submit" className="submit-button" disabled={isDisabled} value="Sign up" />
          </div>
        </form>
      </div>
    );
  }
}

import { connect } from 'react-redux';
import { signupUser } from '../actions/auth_actions';

function mapDispatchToProps(dispatch) {
  return {
    onSignupUser: (signupData) => dispatch(signupUser(signupData))
  };
}

const SignupContainer = connect(null, mapDispatchToProps)(Signup);

export default SignupContainer;