import React from 'react';

const Search = (props) => {
    return (
        <input className={props.class} placeholder="Search" type="text" onChange={props.onSearch}/>
    )
}

export default Search;