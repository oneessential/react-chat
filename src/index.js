import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppContainer from './App';
import store from './store/store';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import './app.css';

ReactDOM.render(
  <Provider store={store}>
    <div className="content-wrapper">
      <AppContainer/>
      <ToastContainer autoClose={2500} hideProgressBar={true} position="bottom-right"/>
    </div>
  </Provider>,
  document.getElementById('root')
);
