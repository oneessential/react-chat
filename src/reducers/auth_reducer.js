import ActionTypes from '../constants/action_types';

export default function (state = {user: {}}, action) {
  switch(action.type) {
    case ActionTypes.LOGIN_USER_SUCCESS:
      return Object.assign({}, state, {
        user: action.user
      })
    case ActionTypes.LOGOUT_USER:
      localStorage.removeItem('userData');
      window.location.reload()
      return Object.assign({}, state, {
        user: {}
      })
    case ActionTypes.SIGNUP_USER_SUCCESS:
      return Object.assign({}, state, {
        user: action.user
      })
    default:
      return state;
  }
}