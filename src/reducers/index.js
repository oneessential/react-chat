import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import authReducer from './auth_reducer';
import roomsReducer from './rooms_reducer';
import usersReducer from './users_reducer';
import messagesReducer from './messages_reducer';

const rootReducer = combineReducers({
  form: reduxFormReducer,
  auth: authReducer,
  rooms: roomsReducer,
  users: usersReducer,
  messages: messagesReducer
});

export default rootReducer;