import ActionTypes from '../constants/action_types';

export default function (state = {messages: []}, action) {
  switch(action.type) {
    case ActionTypes.FETCH_MESSAGES_LOADING:
      return Object.assign({}, state, {
        isLoading: action.isLoading
      })
    case ActionTypes.FETCH_MESSAGES_SUCCESS:
      return Object.assign({}, state, {
        messages: action.messages
      })
    case ActionTypes.ON_MESSAGE: 
      return Object.assign({}, state, {
        messages: [...state.messages, action.message]
      })
    case ActionTypes.JOIN_CHAT:
      console.log(action.username + " joined chat")
      return state;
    case ActionTypes.LEAVE_CHAT:
      console.log(action.username + " leave chat")
      return state;
    default:
      return state;
  }
}