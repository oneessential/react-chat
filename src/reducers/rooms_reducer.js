import ActionTypes from '../constants/action_types';

export default function (state = {rooms: [], unread: []}, action) {
  switch(action.type) {
    case ActionTypes.FETCH_ROOMS_LOADING:
      return Object.assign({}, state, {
        isLoading: action.isLoading
      })
    case ActionTypes.FETCH_ROOMS_SUCCESS:
      return Object.assign({}, state, {
        rooms: action.rooms
      })
    // case ActionTypes.CREATE_ROOM_LOADING:
    //   return Object.assign({}, state, {
    //     isLoading: action.isLoading
    //   })
    case ActionTypes.CREATE_ROOM_SUCCESS:
      return state;
    case ActionTypes.DELETE_ROOM_SUCCESS:
      return Object.assign({}, state, {
        deleted: action.status
      })
    case ActionTypes.UNREAD_MESSAGES:
      return Object.assign({}, state, {
        unread: [...state.unread ,action.roomId]
      })
    case ActionTypes.READ_MESSAGES:
      return Object.assign({}, state, {
        unread: [...state.unread.filter(roomId => roomId !== action.roomId)]
      })
    case ActionTypes.FETCH_ONLINE_SUCCESS:
      return Object.assign({}, state, {
        online: action.online
      })
    default:
      return state;
  }
}