import ActionTypes from '../constants/action_types';

export default function (state = {users: []}, action) {
  switch(action.type) {
    case ActionTypes.FETCH_USERS_LOADING:
      return Object.assign({}, state, {
        isLoading: action.isLoading
      })
    case ActionTypes.FETCH_USERS_SUCCESS:
      return Object.assign({}, state, {
        users: action.users
      })
    default:
      return state;
  }
}